package com.khofifahmaudiirawati_10191044.pratikum2_belajarintent;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button);
        final ImageView imageview = findViewById(R.id.imageview);
        final RelativeLayout relativeLayout = findViewById(R.id.layoutMain);
        button.setOnClickListener(new View.OnClickListener() {
            boolean turnOn;
            @Override
            public void onClick(View view) {
                if (!turnOn){
                    imageview.setImageResource(R.drawable.trans_on);
                    ((TransitionDrawable)imageview.getDrawable()).startTransition(500);
                    relativeLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    turnOn = true;
                }
                else {
                    imageview.setImageResource(R.drawable.trans_off);
                    ((TransitionDrawable)imageview.getDrawable()).startTransition(500);
                    relativeLayout.setBackgroundColor(Color.parseColor("#000000"));
                    turnOn = false;
                }
            }
        });

    }
}